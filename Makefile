scripts = $(wildcard scripts/*.sh)
IMG = scripts/raspbian_lite_static.img
IMG_CACHE = scripts/raspbian_lite_latest.img

BUILD_DIR = build
BUILD_BASE_NAME = raspbian_lite_static
BUILD_IMAGE = $(BUILD_BASE_NAME).img
BUILD_IMAGE_ZIP = $(BUILD_BASE_NAME).zip

CACHE_DIR = cache
CACHE_IMAGE = raspbian_lite_static.img

MNT_DIR = mnt

# enables "make image" to build and perhaps handle caching
.PHONY : image
image : $(BUILD_DIR)/$(BUILD_IMAGE_ZIP)
	echo "updating base image"

# file downloaded from raspberry
$(CACHE_DIR)/$(CACHE_IMAGE) :
	bash ./scripts/download_and_unzip.sh $(CACHE_DIR) $(CACHE_IMAGE);

# build step - take image and apply scripts to it
$(BUILD_DIR)/$(BUILD_IMAGE_ZIP) : $(CACHE_DIR)/$(CACHE_IMAGE) scripts/*.sh
	if [ ! -d $(BUILD_DIR) ]; then \
		mkdir -p $(BUILD_DIR); \
		cp $(CACHE_DIR)/$(CACHE_IMAGE) $(BUILD_DIR)/$(BUILD_IMAGE); \
	else \
		echo "$(BUILD_DIR) already exists, will just update"; \
		echo "use 'make clean' to start over"; \
	fi

	echo "** mount and update"
	bash ./scripts/mount_and_update.sh $(BUILD_DIR)/$(BUILD_IMAGE)

	echo "** unmount and zip"
	bash ./scripts/unmount_and_zip.sh $(BUILD_DIR)/$(BUILD_IMAGE) $(BUILD_DIR)/$(BUILD_IMAGE_ZIP)

	echo "** calculate sha256"
	cd $(BUILD_DIR); sha256sum $(BUILD_IMAGE_ZIP) > $(BUILD_BASE_NAME).sha256

# enables "make cache" to download
.PHONY : cache
cache : $(CACHE_DIR)/$(CACHE_IMAGE)
	echo "cache check"

# enables "make cache-clean" to delete cache dir and redo most things
.PHONY : clean-cache
clean-cache :
	rm -rf $(CACHE_DIR)

# enables "make cache-clean" to delete cache dir and redo most things
.PHONY : clean-build
clean-build :
	rm -rf $(BUILD_DIR)

# "make clean" to remove everything and start over
.PHONY : clean
clean :
	echo "Cleaning cache and build"
	rm -rf $(BUILD_DIR)
	rm -rf $(CACHE_DIR)
	rm -rf $(MNT_DIR)

.PHONY : help
help : Makefile
	@sed -n 's/^##//p' $<
