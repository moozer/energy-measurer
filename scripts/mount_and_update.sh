#!/usr/bin/env bash

# exit if anything fails.
set -e
set -u

if [ "$#" -ne 1 ]; then
    echo "usage $0 <partition image>"
    exit 1
fi

MNTDIR="mnt"
FULL_IMG="$1"

echo "mounting $FULL_IMG to $MNTDIR"
mkdir -p "$MNTDIR"

if command -v guestmount  >> /dev/null; then
  guestmount -a "$FULL_IMG" -m /dev/sda2 -m /dev/sda1:/boot "$MNTDIR"
else
  echo "'guestmount' is not available - using 'mount -o loop ...' "
  bash scripts/loop_mount.sh "$FULL_IMG"
fi

# copy files needed to build image
bash scripts/copy_files.sh

echo "copy update script"
TARGETPATH=home/pi/update_image.sh
cp -v scripts/update_image.sh "$MNTDIR/$TARGETPATH"

echo " - applying update"
proot -0 -r "$MNTDIR" -w / -b /dev -b /sys -b /proc -q qemu-arm-static /bin/bash "/$TARGETPATH"
rm -f "$MNTDIR/$TARGETPATH"
