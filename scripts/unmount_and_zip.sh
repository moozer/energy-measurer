#!/usr/bin/env bash

# exit if anything fails.
set -e
set -u

if [ "$#" -ne 2 ]; then
  echo "usage: $0 <image name> <zip file>"
  exit 1
fi

IMAGE_FILE="$1"
ZIP_FILE="$2"
MNTDIR="mnt"

echo "unmounting $MNTDIR"
if command -v guestmount >> /dev/null; then
  guestunmount "$MNTDIR"
else
  umount "$MNTDIR/boot"
  umount "$MNTDIR"
fi

echo "zipping $IMAGE_FILE to $ZIP_FILE"
if [ -e "$ZIP_FILE" ]; then rm "$ZIP_FILE"; fi
zip "$ZIP_FILE" "$IMAGE_FILE"
