#!/usr/bin/env bash

set -e
set -u

if [ "$#" -ne 1 ]; then
	echo "usage $0 <new image name>"
	exit 1
fi

echo "combining to $1"
cat raspberry-latest-img/part?.img > "$1"
